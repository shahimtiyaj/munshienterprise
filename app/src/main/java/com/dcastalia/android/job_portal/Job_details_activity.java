package com.dcastalia.android.job_portal;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

/**
 * Created by nusrat-pc on 12/18/16.
 */
public class Job_details_activity extends AppCompatActivity{

    Button btn_search_now;
    Context context;
    public static boolean isToastShown = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //For Full Screen view----------------------
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);



        ActionBar mActionBar = getSupportActionBar();
        mActionBar.setHomeButtonEnabled(true);
        mActionBar.setDisplayHomeAsUpEnabled(true);

        mActionBar.setBackgroundDrawable(new ColorDrawable(0xff00DDED));
        mActionBar.setDisplayShowTitleEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(true);

//        // Show the Actionbar in the activity
//        ActionBar actionBar = getSupportActionBar();
//        actionBar.setHomeButtonEnabled(true);
//        actionBar.setDisplayHomeAsUpEnabled(true);
        //actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#000")));

        setTitle("Job Details");
        // getActionBar().setDisplayHomeAsUpEnabled(true);

        super.onCreate(savedInstanceState);


        setContentView(R.layout.job_details_layout);



    }


}
