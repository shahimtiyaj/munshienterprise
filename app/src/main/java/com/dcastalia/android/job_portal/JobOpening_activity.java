package com.dcastalia.android.job_portal;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

/**
 * Created by nusrat-pc on 12/15/16.
 */
public class JobOpening_activity extends AppCompatActivity{
    Button btn_search;
    Context context;
    public static boolean isToastShown=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //For Full Screen view----------------------
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // Show the Actionbar in the activity
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        setTitle("Job Opening");
       // getActionBar().setDisplayHomeAsUpEnabled(true);

        super.onCreate(savedInstanceState);


        setContentView(R.layout.job_open_list_item);

        btn_search=(Button)findViewById(R.id.btn_search);

        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(JobOpening_activity.this,Job_search_activity.class);
                startActivity(intent);
            }
        });



    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
        super.finish();

        Intent intent = new Intent(JobOpening_activity.this, MainActivity.class);
        startActivity(intent);

//        Intent intent = new Intent(JobOpening_activity.this, MainActivity.class);
//        intent.putExtra("now","details");
//        startActivity(intent);
//        finish();


    }


}
