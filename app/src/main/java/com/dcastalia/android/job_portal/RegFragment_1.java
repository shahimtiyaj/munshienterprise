package com.dcastalia.android.job_portal;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;


public class RegFragment_1 extends Fragment implements View.OnClickListener {

    Button btn_reg1;
    String catagory;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.reg_fragment_1, container, false);



        btn_reg1=(Button)view.findViewById(R.id.btn_reg1);

//        btn_reg1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                    radioClick(v);
//
//                    Fragment fragment = new RegFragment_2();
//                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
//                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                    fragmentTransaction.setCustomAnimations(R.anim.left_in, R.anim.left_out);
//                    fragmentTransaction.replace(R.id.fragment_container, fragment);
//                    fragmentTransaction.addToBackStack(null);
//                    fragmentTransaction.commit();
//
//
//
//
//            }
//        });


        /*
    radio button on click event listener
     */
        view.findViewById(R.id.radio_individual_bt).setOnClickListener(this);
        view.findViewById(R.id.radio_agent_bt).setOnClickListener(this);

        return view;
    }


    public void radioClick(View v) {
        boolean checked = ((RadioButton) v).isChecked();

        switch (v.getId()) {
            case R.id.radio_individual_bt:
                if (checked) {

                    btn_reg1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {


                            Fragment fragment = new RegFragment_2();
                            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.setCustomAnimations(R.anim.left_in, R.anim.left_out);
                            fragmentTransaction.replace(R.id.fragment_container, fragment);
                            fragmentTransaction.addToBackStack(null);
                            fragmentTransaction.commit();




                        }
                    });
                    catagory = "individual";
                }
                break;

            case R.id.radio_agent_bt:
                if (checked) {
                    btn_reg1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {


                            Fragment fragment = new SetPaas_withAgent_frag();
                            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.setCustomAnimations(R.anim.left_in, R.anim.left_out);
                            fragmentTransaction.replace(R.id.fragment_container, fragment);
                            fragmentTransaction.addToBackStack(null);
                            fragmentTransaction.commit();




                        }
                    });
                    catagory = "agent";
                }
                break;
            default:
                break;
        }

    }

    /*
    radio button on click method
     */
    @Override
    public void onClick(View v) {
        radioClick(v);
    }
}